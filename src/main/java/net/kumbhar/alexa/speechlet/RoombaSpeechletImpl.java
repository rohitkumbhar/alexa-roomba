/*
 * Copyright 2015 Rohit Kumbhar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kumbhar.alexa.speechlet;

import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.*;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.amazon.speech.ui.SimpleCard;
import com.google.inject.Inject;
import net.kumbhar.alexa.roomba.RoombaInterface;
import net.kumbhar.alexa.user.UserInterface;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;


public class RoombaSpeechletImpl implements RoombaSpeechlet {

    private static final Logger log = LoggerFactory.getLogger(RoombaSpeechletImpl.class);


    private UserInterface userInterface;
    private RoombaInterface roombaInterface;

    @Inject
    public RoombaSpeechletImpl(UserInterface userInterface, RoombaInterface roombaInterface) {

        this.userInterface = userInterface;
        this.roombaInterface = roombaInterface;
    }

    @Override
    public void onSessionStarted(final SessionStartedRequest request, final Session session)
            throws SpeechletException {
        log.info("onSessionStarted requestId={}, sessionId={}", request.getRequestId(), session.getSessionId());
    }

    @Override
    public SpeechletResponse onLaunch(final LaunchRequest request, final Session session)
            throws SpeechletException {

        log.info("onLaunch requestId={}, sessionId={}", request.getRequestId(), session.getSessionId());
        String speechOutput = "Welcome to Roomba integration.";

        // Check for registration
        try {
            boolean isRegistered = userInterface.checkRegistration(session.getUser());

            if (!isRegistered) {
                speechOutput = "Welcome to Roomba integration. It appears that you have not registered. Would you like to register";
                return buildSpeechletResponse(speechOutput, false);
            }

        } catch (SQLException e) {
            throw new SpeechletException(e.getMessage());
        }

        // Here we are setting shouldEndSession to false to not end the session and
        // prompt the user for input
        return buildSpeechletResponse(speechOutput, true);
    }


    @Override
    public SpeechletResponse onIntent(final IntentRequest request, final Session session)
            throws SpeechletException {

        log.info("onIntent requestId={}, sessionId={}, userId={}", request.getRequestId(), session.getSessionId(), session.getUser().getUserId());

        Intent intent = request.getIntent();
        String intentName = (intent != null) ? intent.getName() : null;

        try {
            if (StringUtils.equals("RegistrationIntent", intentName)) {
                return doRegister(session);
            } else if (StringUtils.equals("RegistrationQueryIntent", intentName)) {
                String message = userInterface.checkRegistration(session.getUser()) ? "You are registered" : "You are not registered";
                return buildSpeechletResponse(message, true);
            } else if (StringUtils.equals("RegistrationDeleteIntent", intentName)) {
                return deleteRegistration(session);
            } else if (StringUtils.equals("RoombaBeepIntent", intentName)) {
                final String message = roombaInterface.beep(session.getUser());
                return buildSpeechletResponse(message, true);
            } else if (StringUtils.equals("RoombaSleepIntent", intentName)) {
                final String message = roombaInterface.sleep(session.getUser());
                return buildSpeechletResponse(message, true);
            } else if (StringUtils.equals("RoombaCleanIntent", intentName)) {
                final String message = roombaInterface.clean(session.getUser());
                return buildSpeechletResponse(message, true);
            } else if (StringUtils.equals("RoombaDockIntent", intentName)) {
                final String message = roombaInterface.dock(session.getUser());
                return buildSpeechletResponse(message, true);
            } else {
                return buildSpeechletResponse("Replace this with a help message", true);
            }
        } catch (Exception e) {
            throw new SpeechletException(e.getMessage(), e);
        }
    }

    private SpeechletResponse deleteRegistration(Session session) throws SQLException {

        final boolean deleteRegistration = userInterface.deleteRegistration(session.getUser());

        if (deleteRegistration) {
            return buildSpeechletResponse("Your registration has been deleted", true);
        } else {
            return buildSpeechletResponse("There was an error deleting your registration. Please try again after later.", true);
        }


    }

    private SpeechletResponse doRegister(Session session) throws SQLException {

        final String registrationKey = userInterface.registerUser(session.getUser());
        final String deviceRegistrationUrl = "<a href=\"boomba://registration/" + registrationKey + "\">Device registration</a>";
        final String cardContent = "Your device activation code is " + registrationKey + ". Please enter the code in the companion app.";
        return buildSpeechletResponse(cardContent + " Launch app with " + deviceRegistrationUrl, cardContent, true);
    }


    @Override
    public void onSessionEnded(final SessionEndedRequest request, final Session session)
            throws SpeechletException {
        log.info("onSessionEnded requestId={}, sessionId={}", request.getRequestId(),
                session.getSessionId());
    }


    /**
     * Helper method when the card and speech content are the same
     *
     * @param speechContent    Content for speech
     * @param shouldEndSession Should the session be closed
     * @return SpeechletResponse spoken and visual response for the given input
     */
    private SpeechletResponse buildSpeechletResponse(final String speechContent, final boolean shouldEndSession) {
        return buildSpeechletResponse(speechContent, speechContent, shouldEndSession);
    }

    /**
     * Creates and returns the visual and spoken response with shouldEndSession flag.
     *
     * @param cardContent      Content for companion application home card
     * @param speechContent    Content for speech
     * @param shouldEndSession Should the session be closed
     * @return SpeechletResponse spoken and visual response for the given input
     */
    private SpeechletResponse buildSpeechletResponse(final String cardContent,
                                                     final String speechContent, final boolean shouldEndSession) {
        // Create the Simple card content.
        SimpleCard card = new SimpleCard();
        card.setTitle("Roomba Integration");
        card.setContent(String.format("%s", cardContent));

        // Create the plain text cardContent.
        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText(speechContent);

        // Create the speechlet response.
        SpeechletResponse response = new SpeechletResponse();
        response.setShouldEndSession(shouldEndSession);
        response.setOutputSpeech(speech);
        response.setCard(card);
        return response;
    }
}
