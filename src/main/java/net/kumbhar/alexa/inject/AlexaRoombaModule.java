/*
 * Copyright 2015 Rohit Kumbhar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kumbhar.alexa.inject;

import com.google.inject.AbstractModule;
import net.kumbhar.alexa.db.DatabaseService;
import net.kumbhar.alexa.db.impl.SQLiteDatabaseService;
import net.kumbhar.alexa.roomba.RoombaInterface;
import net.kumbhar.alexa.roomba.RoombaOverGoogleCloudMessaging;
import net.kumbhar.alexa.speechlet.RoombaSpeechlet;
import net.kumbhar.alexa.speechlet.RoombaSpeechletImpl;
import net.kumbhar.alexa.user.SimpleUserInterface;
import net.kumbhar.alexa.user.UserInterface;

/**
 * Guice injection module
 */
public class AlexaRoombaModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(RoombaSpeechlet.class).to(RoombaSpeechletImpl.class);
        bind(DatabaseService.class).to(SQLiteDatabaseService.class);
        bind(UserInterface.class).to(SimpleUserInterface.class);
        bind(RoombaInterface.class).to(RoombaOverGoogleCloudMessaging.class);
    }
}
