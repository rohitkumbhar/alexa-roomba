package net.kumbhar.alexa.db.impl;

import net.kumbhar.alexa.db.DatabaseService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Database service backed by SQLite
 */
public class SQLiteDatabaseService implements DatabaseService {

    public static final String SQLITE_DB_URL = "jdbc:sqlite:/home/rohit/alexa-roomba.db";
    public SQLiteDatabaseService() throws SQLException, ClassNotFoundException {
        Class.forName("org.sqlite.JDBC");
    }

    @Override
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(SQLITE_DB_URL);
    }

}
