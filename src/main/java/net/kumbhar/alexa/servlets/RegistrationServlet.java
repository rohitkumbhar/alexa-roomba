/*
 * Copyright 2015 Rohit Kumbhar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kumbhar.alexa.servlets;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import net.kumbhar.alexa.user.UserInterface;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@Singleton
public class RegistrationServlet extends HttpServlet {

    private static final Logger log = LoggerFactory.getLogger(RegistrationServlet.class);
    private UserInterface userInterface;

    @Inject
    public RegistrationServlet(UserInterface userInterface) {
        this.userInterface = userInterface;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String registrationKey = StringUtils.defaultString(request.getParameter("registration_key"), StringUtils.EMPTY);
        String gcmId = StringUtils.defaultString(request.getParameter("gcm_id"), StringUtils.EMPTY);

        boolean status = false;
        String message;
        try {
            status = userInterface.updateGcmPairingId(registrationKey, gcmId);
            message = "Result: " + status;
        } catch (SQLException e) {
            message = "Fail: " + e.getMessage();
        }

        if (status) {
            log.info("Successful pairing for {}", registrationKey);
            response.setStatus(200);
            response.getOutputStream().print(message);
        } else {
            log.warn("Failed pairing for {}", registrationKey);
            response.setStatus(500);
            response.getOutputStream().print(message);
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        throw new ServletException("Get is not supported");
    }
}
