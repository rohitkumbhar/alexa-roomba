/*
 * Copyright 2015 Rohit Kumbhar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kumbhar.alexa.roomba;


import com.amazon.speech.speechlet.User;
import com.google.inject.Inject;
import net.kumbhar.alexa.user.SimpleUser;
import net.kumbhar.alexa.user.UserInterface;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.time.ZonedDateTime;
import java.util.Properties;

public class RoombaOverGoogleCloudMessaging implements RoombaInterface {

    private static final Logger log = LoggerFactory.getLogger(RoombaOverGoogleCloudMessaging.class);
    private UserInterface userInterface;

    @Inject
    public RoombaOverGoogleCloudMessaging(UserInterface userInterface) {
        this.userInterface = userInterface;
    }

    private String executeCommand(User user, String command) throws IOException {

        try {

            final SimpleUser registeredUser = userInterface.getRegisteredUser(user.getUserId());

            if (registeredUser == null) {
                return "You are not registered to use this skill. Please ask roomba to register you.";
            }

            final String gcmToken = registeredUser.getGcmToken();

            if (gcmToken == null || gcmToken.isEmpty()) {
                return "Your phone has not paired with your roomba yet. Please update your pairing code";
            }

            final JSONObject message = buildCommandMessage(gcmToken, command);

            // Call GCDM
            CloseableHttpResponse response = sendMessage(message);

            // Process response
            HttpEntity responseEntity = response.getEntity();
            String responseString = EntityUtils.toString(responseEntity);
            StatusLine statusLine = response.getStatusLine();

            if (statusLine.getStatusCode() != 200) {
                log.error("Error from GCM call: {}, with response line: ", responseString);
                return "An error occurred while contacting your Roomba. Please try again later.";
            }

            log.info("Response from server: {} ", responseString);
            return "Asking Roomba to " + command;

        } catch (SQLException e) {
            log.error("SQLException when fetching the user: {}", e.getMessage(), e);
            return "An error occurred while contacting your Roomba. Please try again later.";
        }


    }

    /**
     * Send message via Google Cloud Messaging to the paired devide
     *
     * @param message Message string
     * @return HTTPResponse object
     * @throws IOException
     */
    private CloseableHttpResponse sendMessage(JSONObject message) throws IOException {

        HttpEntity entity = new StringEntity(message.toString(), ContentType.APPLICATION_JSON);
        HttpUriRequest request = RequestBuilder.post()
                .setUri("https://gcm-http.googleapis.com/gcm/send")
                .setHeader("Content-Type", "application/json")
                .setHeader("Authorization", "key=" + getServerKey())
                .setEntity(entity).build();

        CloseableHttpClient httpClient = HttpClients.createDefault();
        return httpClient.execute(request);
    }

    private String getServerKey() {


        String serverKey = System.getProperty("gcm.server.api.key");
        if (serverKey == null) {
            try {
                Properties props = new Properties();
                InputStream resourceAsStream = RoombaOverGoogleCloudMessaging.class.getResourceAsStream("/gcm-server.properties");
                props.load(resourceAsStream);
                serverKey = props.getProperty("gcm.server.api.key");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (serverKey == null) {
            throw new RuntimeException("GCM Server key not found");
        }

        return serverKey;
    }

    /**
     * Build a JSONObject of the command to be sent to the device
     *
     * @param gcmToken Application token
     * @param command  Command string
     * @return JSONObject of the command
     */
    private JSONObject buildCommandMessage(String gcmToken, String command) {

        JSONObject data = new JSONObject();
        data.put("roomba_command", command);
        data.put("server_time", ZonedDateTime.now().toString());

        JSONObject message = new JSONObject();
        message.put("to", gcmToken);
        message.put("data", data);
        return message;
    }

    @Override
    public String beep(User user) throws IOException {
        return executeCommand(user, "beep");
    }

    @Override
    public String sleep(User user) throws IOException {
        return executeCommand(user, "sleep");
    }

    @Override
    public String clean(User user) throws IOException {
        return executeCommand(user, "clean");
    }

    @Override
    public String dock(User user) throws IOException {
        return executeCommand(user, "dock");
    }
}
