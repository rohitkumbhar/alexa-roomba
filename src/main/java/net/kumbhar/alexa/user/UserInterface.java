/*
 * Copyright 2015 Rohit Kumbhar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kumbhar.alexa.user;

import com.amazon.speech.speechlet.User;

import java.sql.SQLException;

public interface UserInterface {


    /**
     * Delete the registration record for this user
     *
     * @param user Amazon User
     * @return true if the deletion is successful
     * @throws SQLException
     */
    boolean deleteRegistration(User user) throws SQLException;

    /**
     * Register this user
     *
     * @param user Amazon User
     * @return A registration key identifying the user to be used as the pairing code with GCM
     * @throws SQLException
     */
    String registerUser(User user) throws SQLException;

    /**
     * Check if the user is registered
     *
     * @param user Amazon User
     * @return true if the user's record exists in the system
     * @throws SQLException
     */
    boolean checkRegistration(User user) throws SQLException;

    /**
     * Update the Google Cloud Messaging id for the user's device with the Echo's user id.
     *
     * @param registrationKey Pairing key provided to the user
     * @param gcmId           User's GCM registration id
     * @return true if the update was successful
     * @throws SQLException
     */
    boolean updateGcmPairingId(String registrationKey, String gcmId) throws SQLException;

    /**
     * Get the registered user for the user id
     *
     * @param userId Echo's user id
     * @return An instance of SimpleUser if the user exists, null otherwise
     * @throws SQLException
     */
    SimpleUser getRegisteredUser(String userId) throws SQLException;
}
