/*
 * Copyright 2015 Rohit Kumbhar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kumbhar.alexa.user;

import com.amazon.speech.speechlet.User;
import com.google.inject.Inject;
import net.kumbhar.alexa.db.DatabaseService;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.UUID;

public class SimpleUserInterface implements UserInterface {

    private static final Logger log = LoggerFactory.getLogger(SimpleUserInterface.class);

    public static final String INSERT_INTO_STATEMENT = "insert into registrations(user_id, salt, user_email, gcdm_registration_id, registration_key) ";

    public static final String DELETE_USER_STATEMENT = "DELETE from registrations where user_id = ?";
    public static final String GET_USER_FOR_USER_ID = "select * from registrations where user_id = ?";
    public static final String GET_USER_FOR_REGISTRATION_KEY = "select * from registrations where registration_key = ?";
    public static final String UPDATE_GCM_ID = "update registrations set gcdm_registration_id = ? where registration_key = ? and user_id = ?";
    private DatabaseService databaseService;

    @Inject
    public SimpleUserInterface(DatabaseService databaseService) {
        this.databaseService = databaseService;
    }


    @Override
    public boolean deleteRegistration(User user) throws SQLException {
        try (final Connection connection = databaseService.getConnection();
             final PreparedStatement ps = connection.prepareStatement(DELETE_USER_STATEMENT)) {
            ps.setString(1, user.getUserId());
            ps.executeUpdate();
            return true;
        }
    }

    @Override
    public String registerUser(User user) throws SQLException {

        try (final Connection connection = databaseService.getConnection();
             final Statement statement = connection.createStatement()) {

            final String salt = UUID.randomUUID().toString();
            final String userId = user.getUserId();

            String registrationKey = null;
            boolean availableKey = true;

            while (availableKey) {
                registrationKey = RandomStringUtils.randomAlphanumeric(10);
                PreparedStatement regKeyQuery = connection.prepareStatement(GET_USER_FOR_REGISTRATION_KEY);
                regKeyQuery.setString(1, registrationKey);
                ResultSet resultSet = regKeyQuery.executeQuery();
                availableKey = resultSet.next();
            }

            registrationKey = registrationKey.toUpperCase();
            statement.executeUpdate(INSERT_INTO_STATEMENT + "VALUES ('" + userId + "','" + salt + "',null,null,'" + registrationKey + "'  )");
            return registrationKey;

        }
    }

    @Override
    public boolean checkRegistration(User user) throws SQLException {

        try (final Connection connection = databaseService.getConnection();
             final PreparedStatement ps = connection.prepareStatement(GET_USER_FOR_USER_ID)) {
            ps.setString(1, user.getUserId());
            final ResultSet resultSet = ps.executeQuery();
            return resultSet.next();
        }
    }

    @Override
    public boolean updateGcmPairingId(String registrationKey, String gcmId) throws SQLException {

        try (final Connection connection = databaseService.getConnection();
             final PreparedStatement gps = connection.prepareStatement(GET_USER_FOR_REGISTRATION_KEY);
             final PreparedStatement ups = connection.prepareStatement(UPDATE_GCM_ID)) {

            registrationKey = registrationKey.toUpperCase();
            gps.setString(1, registrationKey);
            final ResultSet resultSet = gps.executeQuery();
            if (resultSet.next()) {
                final String userid = resultSet.getString("user_id");
                log.info("Updating gcm id for userId {} and registrationKey {} with gcm pairing id {}", userid, registrationKey, gcmId);

                ups.setString(1, gcmId);
                ups.setString(2, registrationKey);
                ups.setString(3, userid);
                int numberOfRecordsUpdate = ups.executeUpdate();
                log.debug("Number of records updated: {}", numberOfRecordsUpdate);

                return true;

            } else {
                log.warn("Cannot update pairing status for {} since there is no registration key in the database.", registrationKey);
                return false;
            }
        }
    }

    @Override
    public SimpleUser getRegisteredUser(String userId) throws SQLException {

        try (final Connection connection = databaseService.getConnection();
             final PreparedStatement ps = connection.prepareStatement(GET_USER_FOR_USER_ID)) {

            ps.setString(1, userId);
            final ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                return new SimpleUser(resultSet.getString("user_id"),
                        resultSet.getString("gcdm_registration_id"),
                        resultSet.getString("registration_key"));
            }
            return null;
        }
    }
}
